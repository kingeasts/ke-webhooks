FROM php:7.2-cli
ENV BUILD_DIR /build
ENV IS_DOCKER ke

COPY ./web /web

RUN docker-php-source extract \
    && apt-get update \
    && apt-get install -y git \
    && mkdir $BUILD_DIR \
    && cd $BUILD_DIR \
    # ---------------------------------------
    # swoole
    # ---------------------------------------
    && curl -L -o $BUILD_DIR/swoole.tgz 'http://pecl.php.net/get/swoole-4.2.5.tgz' \
    && tar zxvf swoole.tgz \
    && cd swoole-4.2.5 \
    && phpize \
    && ./configure \
    && make && make install \
    && echo 'extension=swoole.so' >> /usr/local/etc/php/conf.d/docker-php-ext-swoole.ini \
    && echo 'swoole complete!' \
    # ----------------------------------------
    # composer
    # ----------------------------------------
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && composer config -g repo.packagist composer https://packagist.laravel-china.org \
    && echo 'composer complete!' \
    && docker-php-source delete

WORKDIR /web
EXPOSE 10188
ENTRYPOINT ["php", "/web/boot.php"]