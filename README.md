### git-webhooks

**配置文件[config.php]**

```
config.php
```

**启动服务**

> 建议使用docker部署

```
php boot.php
// 需要注意的是，git pull的失败以旧会返回success，详情需要看logs
```

**创建部署任务**

```
进入项目根目录执行php create name
// 执行完毕会创建/data/xxx.php文件，返回的URL就是钩子名称
```

**docker下使用**

```
docker run -it -d -p 10188:10188 -e KE_DOMAIN=127.0.0.1 -v your_path/www:/web/www wdaglb/webhooks
```
