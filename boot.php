#!/usr/bin/env php
<?php

if (isset($_ENV['IS_DOCKER']) && $_ENV['IS_DOCKER'] === 'ke') {
    $config = require __DIR__ . '/docker.php';
} else {
    $config = require __DIR__ . '/config.php';
}

define('ROOT_PATH', __DIR__ . '/');
require ROOT_PATH . 'src/Build.php';

// 初始化构建类
$build = new Build();

// 初始化http
$http = new Swoole\Http\Server('0.0.0.0', $config['port']);

echo "webhooks start success!\n";
$http->on('request', function ($request, $response) use($build) {
    if (empty($request->get['name'])) {
        return $response->end('name Non-exist!');
    }
    if (empty($request->get['token'])) {
        return $response->end('token Non-exist!');
    }
    go(function () use($request, $build) {
        $build->run($request);
    });
    $response->end('success');
});

$http->start();
